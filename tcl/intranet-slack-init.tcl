# /packages/intranet-slack/tcl/intranet-slack-init.tcl
ad_library {
    Initialization code
}

if { ![nsv_exists . slack_wd_installed_p] } {
    nsv_set . slack_wd_installed_p 0
}

if { ![nsv_get . slack_wd_installed_p] } {
    ad_schedule_proc [expr 60 * [parameter::get_from_package_key -package_key "intranet-slack" -parameter WatchDogSlackFrequency]] {slack::post_wd_errors}
    nsv_set . slack_wd_installed_p 1
}
